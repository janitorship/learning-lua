--[[
    Making of hello world program.
]]

-- Print a statement
print("Hello World.")

-- Declare a global variable
A = 5 -- Uppercase initial for global variables
print(A)

-- Declare a local variable
local a = 5
print(a)

-- Print type of a varibale
print(type(a))

-- What is nothing, is nil
a = nil
print(a, nil)

-- Booleans
a = true
print(type(a), a)

-- Strings

-- replacement
A = "you have shit your pants."
--              V - string variable with text to be replaced
B = string.gsub(A, "shit", "####")
-- text to be replaced ^  ^ the text to replace it with
print(A)
print(B)

-- printing multiple strings

Page = [[
    <HTML>
    <HEAD>
    <TITLE>An HTML Page</TITLE>
    </HEAD>
    <BODY>
     <A HREF="http://www.lua.org">Lua</A>
    </BODY>
    </HTML>
]]

print(Page)

-- conversion of string into numbers
print("10" + 1)
print("10 + 1")
print("-5.3e-10"*"2")
-- print("hello" + 1)

-- magic, just kidding, concatenation
print("a" .. "b") -- the .. is the concatenation operator
print(10 .. 20)

-- Take input from user
Line = io.read()

-- Conversion of a string to number (will try, otherwise nil return)
N = tonumber(Line)
print(Line, N, type(Line), type(N))

-- vice-versa
Line = tostring(N)
print(Line, type(Line))

-- Create a key value pair
a = {}
K = 20
a[K] = 20
print(a[K], a[20])

Cats = {}
Cats["Cat A"] = "Maine Coon"
Cats["Cat B"] = "Himalayan"
print(Cats, Cats["Cat A"], Cats["Cat B"])

-- Make dummy entries
a = {} -- empty tables
-- Loops

-- Make dummy entries
for i=1,100 do
    a[i] = i*2
end

-- Print the entries (the ipairs is responsible for iterating over the array)
for i,line in ipairs(a) do
    print(line)
end

-- Expressions

-- Power
print(3 ^ 2)

-- Yeah, logical operators
Mom = false
Dad = true
Child = Mom and Dad
print(Mom, Dad, Child)

-- Precedence
--[[
    Completely different from C's precedence table.
    1 - ^
    2 - not  - (unary)
    3 - *   /
    4 - +   -
    5 - ..
    6 - <   >   <=  >=  ~=  ==
    7 - and
    8 - or
]]

-- Table constructors
Days = {"Sundays", "Monday", "Tuesday"}
print(Days[2]) -- Arrays start at 0 *visible confusion8
Days[1] = nil
print(Days[1])

-- List creation from nil, jk, reverse order linked list that doesn't work
List = Days

-- Print values of a list
L = List
while L do
    print(L.value)
    L = L.next
end

-- Constructors

-- Mix Record-style and list-style initializations in the same consturctor
Polyline = {color="blue", thickness=2, npoints=4,
                {x=0, y=0},
                {x=-10, y=0},
                {x=-10, y=1},
                {x=0, y=1}
            }


print(Polyline[2].y)

-- I like the simple way

-- concatenation
A = "hello" .. "world"

-- Swapping variable values
X = "Men"
Y = "Women"
X, Y = Y, X

Population = {Men="1.3B", Women="830M"}
Population["Men"], Population["Women"] = Population["Women"], Population["Men"]
print(Population[1])

-- Control Structures
Toad = "Toad"
Turtle = "Turtle"

if Toad == Turtle then
    print(Toad, Turtle)
else
    print(Turtle, Toad)
end

local i = 1
while a[i] do
  print(a[i])
  i = i + 1
end

-- print the first non-empty line
A = B
repeat
    print(A)
until A == A
print(A, B)

-- For loop is to continue the body of that loop till the intialization statement finishes
for I=1,100 do
    print(I)
end

Names = {"Peter", "Paul", "Mary"}
Grades = {Mary = 10, Paul = 7, Peter = 8}
table.sort(Names, function (n1, n2)
    return Grades[n1] < Grades[n2]
end)

for _, line in ipairs(Names) do
    print(line)
end

-- You can print the table items using this

-- Compilation, Execution, and Errors
function dofile (filename)
    local f = assert(loadfile(filename))
    return f()
end

-- assert() is used for raising an error when a certain operation or statement fails.

-- For causing errors => error("hehe")

local foo = true

if pcall(foo) then
    -- do something, no errors while running 'foo'
    print('something')
else
    -- 'foo' raised an error: take appropriate actions
end

-- Error code
-- V this can be used for printing error code
local status, err = pcall(function () error({code=121}) end)
    print(err.code)  -->  121

-- You can traceback an error by using debug.traceback()
