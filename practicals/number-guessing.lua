require("math")

local seed = math.randomseed(os.time())
local number = math.random(1,50)

while true do
    print("Guess the number: ")
    local guess = io.read()
    guess = tonumber(guess)
    if guess > number then
        print("Too high!")
    elseif guess < number then
        print("Too low!")
    end
end