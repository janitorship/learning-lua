print("Enter any operator ( +, -, /, *, ^, %): ")

local operator = io.read()

print("Enter two numbers: ")

local num1 = io.read()
local num2 = io.read()

num1, num2 = tonumber(num1), tonumber(num2)

if operator == "+" then
    print(num1 + num2)

elseif operator == "-" then
    print(num1 - num2)

elseif operator == "/" then
    print(num1 / num2)

elseif operator == "*" then
    print(num1 * num2)

elseif operator == "^" then
    print(num1 ^ num2)

elseif operator == "%" then
    print(num1 % num2)
end
